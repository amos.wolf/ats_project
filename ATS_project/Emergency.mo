within ATS_project;

model Emergency " La source Emergency " 
  parameter Real V(displayUnit="V") = 5 "Tension source emergency ";
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V = V)  annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_p "Pin positif de la source emergency" annotation(
    Placement(visible = true, transformation(origin = {-104, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_n "Pin negatif de la source emergency" annotation(
    Placement(visible = true, transformation(origin = {104, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, -30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(pin_p, constantVoltage.p) annotation(
    Line(points = {{-104, 0}, {-10, 0}}, color = {0, 0, 255}));
  connect(constantVoltage.n, pin_n) annotation(
    Line(points = {{10, 0}, {104, 0}}, color = {0, 0, 255}));

annotation(
    Icon(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Polygon(origin = {23.33, 0}, fillColor = {255, 0, 0}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-23.333, 30}, {46.667, 0}, {-23.333, -30}, {-23.333, 30}}), Rectangle(fillColor = {255, 0, 0}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-70, -4.5}, {0, 4.5}}), Rectangle(lineColor = {128, 128, 128}, extent = {{-100, -100}, {100, 100}}, radius = 25), Text(origin = {0, -110}, extent = {{-100, 20}, {100, -20}}, textString = "%name")}));
end Emergency;
