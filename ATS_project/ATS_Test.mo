within ATS_project;

model ATS_Test "Demo for the ATS"
  extends Modelica.Icons.Example;
  ATS_project.Load load annotation(
    Placement(visible = true, transformation(origin = {78, 0}, extent = {{-18, -18}, {18, 18}}, rotation = 0)));
  ATS_project.Normal normal(V(displayUnit = ""))  annotation(
    Placement(visible = true, transformation(origin = {-83, -83}, extent = {{-17, -17}, {17, 17}}, rotation = 0)));
  ATS_project.Emergency emergency annotation(
    Placement(visible = true, transformation(origin = {-84, 84}, extent = {{-16, -16}, {16, 16}}, rotation = 0)));
  ATS_project.ATS ats annotation(
    Placement(visible = true, transformation(origin = {-41.3223, 0.677689}, extent = {{-23.6777, -23.6777}, {55.2479, 23.6777}}, rotation = 0)));
equation
  connect(load.pin_n, ats.pin_load_n) annotation(
    Line(points = {{58, -7}, {43, -7}, {43, -2}, {21, -2}}, color = {0, 0, 255}));
  connect(load.pin_p, ats.pin_p2) annotation(
    Line(points = {{58, 7}, {48, 7}, {48, 3}, {21, 3}}, color = {0, 0, 255}));
  connect(emergency.pin_p, ats.pin_p) annotation(
    Line(points = {{-66, 89}, {-61, 89}, {-61, 23}}, color = {0, 0, 255}));
  connect(emergency.pin_n, ats.pin_emergency_n) annotation(
    Line(points = {{-66, 79}, {-61, 79}, {-61, 18}}, color = {0, 0, 255}));
  connect(normal.pin_p, ats.pin_p1) annotation(
    Line(points = {{-65, -75}, {-61, -75}, {-61, -17}}, color = {0, 0, 255}));
  connect(normal.pin_n, ats.pin_normal_n) annotation(
    Line(points = {{-64, -92}, {-61, -92}, {-61, -21}}, color = {0, 0, 255}));
  annotation(
    experiment(StartTime = 0, StopTime = 10, Tolerance = 1e-6, Interval = 0.02));
end ATS_Test;
