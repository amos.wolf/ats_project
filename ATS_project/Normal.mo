within ATS_project;

model Normal "Source normal"
  parameter Real V(displayUnit = "V") = 10 "Tension de l'etat normal";
  parameter Real period=4;
  parameter Real width=50;
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_p annotation(
    Placement(visible = true, transformation(origin = {-96, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {108, 46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_n annotation(
    Placement(visible = true, transformation(origin = {98, -16}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, -52}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.PulseVoltage pulseVoltage(V = V, period = period, startTime = 0, width = width)  annotation(
    Placement(visible = true, transformation(origin = {-6, 18}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(pulseVoltage.p, pin_p) annotation(
    Line(points = {{-16, 18}, {-96, 18}, {-96, -2}}, color = {0, 0, 255}));
  connect(pulseVoltage.n, pin_n) annotation(
    Line(points = {{4, 18}, {98, 18}, {98, -16}}, color = {0, 0, 255}));
  annotation(
    Icon(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Rectangle(fillColor = {128, 128, 128}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{-70, -4.5}, {0, 4.5}}), Rectangle(lineColor = {128, 128, 128}, extent = {{-100, -100}, {100, 100}}, radius = 25), Polygon(origin = {23.3333, 0}, fillColor = {128, 128, 128}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, points = {{-23.333, 30}, {46.667, 0}, {-23.333, -30}, {-23.333, 30}}), Text(origin = {0, -110}, extent = {{-100, 20}, {100, -20}}, textString = "%name")}));
end Normal;
