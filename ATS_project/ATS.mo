within ATS_project;

model ATS "Automatic Transfer Switch"
  parameter Modelica.Units.SI.TranslationalSpringConstant Spring_constant=10 "constant du ressort";
  parameter Modelica.Units.SI.Angle Start_angle=0.52 "rotation initiale de la bielle";
  parameter Modelica.Units.SI.Distance R_bielle(displayUnit="m")=0.02 "rayon de la bielle";
  parameter Modelica.Units.SI.Inertia Inertia_ats=0.04 "inertie du systeme";
  parameter Real Gain_force_sol=-40 "gain qui transforme le voltage de la batterie dans la force du solenoide";
  parameter Modelica.Units.SI.Voltage V_batterie=5 "voltage batterie qui alimente le solenoide";
  parameter Modelica.Units.SI.Distance Spring_rel0=0.06 "longueur du ressort non tendu" ;
  parameter Modelica.Units.SI.Voltage V_min2e=3 "voltage minimum pour switcher de la source normal à emergency";
  parameter Modelica.Units.SI.Voltage V_min2n=5 "voltage minimum pour switcher de la source emergency à normal";
  
  ATS_project.Components.Bielle bielle(R=R_bielle) annotation(
    Placement(visible = true, transformation(origin = {95, -1}, extent = {{-17, -17}, {17, 17}}, rotation = 0)));
  ATS_project.Components.Controlleur controlleur(V1_min=V_min2e,V2_min=V_min2n, tc = 0.15) annotation(
    Placement(visible = true, transformation(origin = {-53, 77}, extent = {{-19, -19}, {19, 19}}, rotation = 0)));
  ATS_project.Components.Batterie batterie(V_batterie=V_batterie) annotation(
    Placement(visible = true, transformation(origin = {-92, 8}, extent = {{-18, -18}, {18, 18}}, rotation = -90)));
  Modelica.Mechanics.Translational.Components.Spring spring(c = Spring_constant,s_rel(start = 0), s_rel0 = Spring_rel0)  annotation(
    Placement(visible = true, transformation(origin = {34, -20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Translational.Components.Fixed fixed annotation(
    Placement(visible = true, transformation(origin = {14, -26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_p annotation(
    Placement(visible = true, transformation(origin = {310, 64}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-160, 140}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_p1 annotation(
    Placement(visible = true, transformation(origin = {314, -26}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-160, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor annotation(
    Placement(visible = true, transformation(origin = {298, -110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_p2 annotation(
    Placement(visible = true, transformation(origin = {312, 12}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {360, 16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ATS_project.Components.Switch switch annotation(
    Placement(visible = true, transformation(origin = {192, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ATS_project.Components.Adapteur adapteur(Start_angle=Start_angle, Inertia_adapteur=Inertia_ats) annotation(
    Placement(visible = true, transformation(origin = {162, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Ideal.IdealOpeningSwitch switch1 annotation(
    Placement(visible = true, transformation(origin = {-16, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_normal_n annotation(
    Placement(visible = true, transformation(origin = {316, -48}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-160, -140}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_load_n annotation(
    Placement(visible = true, transformation(origin = {310, -4}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {360, -16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_emergency_n annotation(
    Placement(visible = true, transformation(origin = {310, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-160, 110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ATS_project.Components.Solenoide_alt solenoide_alt(Gain_force_solenoide=Gain_force_sol) annotation(
    Placement(visible = true, transformation(origin = {21, 13}, extent = {{-17, -17}, {17, 17}}, rotation = 0)));
  Modelica.Mechanics.Translational.Components.Mass mass(m = 0.0001)  annotation(
    Placement(visible = true, transformation(origin = {52, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation(
    Placement(visible = true, transformation(origin = {-72, -56}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Inertia inertia(J = 0.1, phi(displayUnit = "rad")) annotation(
    Placement(visible = true, transformation(origin = {132, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(spring.flange_a, fixed.flange) annotation(
    Line(points = {{24, -20}, {14, -20}, {14, -26}}, color = {0, 127, 0}));
  connect(voltageSensor.v, controlleur.sensor) annotation(
    Line(points = {{298, -121}, {-60, -121}, {-60, 68}}, color = {0, 0, 127}));
  connect(controlleur.y, switch1.control) annotation(
    Line(points = {{-64, 78}, {-16, 78}, {-16, 46}}, color = {255, 0, 255}));
  connect(batterie.pin_p, switch1.p) annotation(
    Line(points = {{-72, 24}, {-26, 24}, {-26, 34}}, color = {0, 0, 255}));
  connect(solenoide_alt.pin_solenoide_p, switch1.n) annotation(
    Line(points = {{2, 21.5}, {-6, 21.5}, {-6, 34}}, color = {0, 0, 255}));
  connect(solenoide_alt.pin_solenoide_n, batterie.pin_n) annotation(
    Line(points = {{2, 4.5}, {-72, 4.5}, {-72, -8}}, color = {0, 0, 255}));
  connect(mass.flange_b, bielle.flange_b) annotation(
    Line(points = {{62, 0}, {71, 0}, {71, -1}, {76, -1}}, color = {0, 127, 0}));
  connect(spring.flange_b, mass.flange_a) annotation(
    Line(points = {{44, -20}, {42, -20}, {42, 0}}, color = {0, 127, 0}));
  connect(solenoide_alt.flange_a, mass.flange_a) annotation(
    Line(points = {{38, 11}, {42, 11}, {42, 0}}, color = {0, 127, 0}));
  connect(pin_p, switch.pin_emergency) annotation(
    Line(points = {{310, 64}, {203, 64}, {203, 11}}, color = {0, 0, 255}));
  connect(switch.pin_emergency_n, pin_emergency_n) annotation(
    Line(points = {{203, 9}, {252, 9}, {252, 50}, {310, 50}}, color = {0, 0, 255}));
  connect(switch.pin_load, pin_p2) annotation(
    Line(points = {{203, 3}, {290, 3}, {290, 12}, {312, 12}}, color = {0, 0, 255}));
  connect(switch.pin_load_n, pin_load_n) annotation(
    Line(points = {{203, 0}, {310, 0}, {310, -4}}, color = {0, 0, 255}));
  connect(switch.pin_normal_n, pin_normal_n) annotation(
    Line(points = {{203, -7}, {210, -7}, {210, -48}, {316, -48}}, color = {0, 0, 255}));
  connect(switch.pin_normal, pin_p1) annotation(
    Line(points = {{203, -4}, {276, -4}, {276, -26}, {314, -26}}, color = {0, 0, 255}));
  connect(ground.p, batterie.pin_n) annotation(
    Line(points = {{-72, -46}, {-72, -8}}, color = {0, 0, 255}));
  connect(voltageSensor.n, pin_normal_n) annotation(
    Line(points = {{308, -110}, {308, -79}, {316, -79}, {316, -48}}, color = {0, 0, 255}));
  connect(voltageSensor.p, pin_p1) annotation(
    Line(points = {{288, -110}, {288, -38}, {314, -38}, {314, -26}}, color = {0, 0, 255}));
  connect(adapteur.em_norm_decision, switch.em_norm_decision) annotation(
    Line(points = {{173, 2}, {180, 2}}, color = {255, 0, 255}));
  connect(bielle.flange_a, inertia.flange_a) annotation(
    Line(points = {{103.5, -1}, {122, -1}, {122, -2}}));
  connect(inertia.flange_b, adapteur.flange_a) annotation(
    Line(points = {{142, -2}, {146.5, -2}, {146.5, 2}, {151, 2}}));
  annotation(
    Icon(coordinateSystem(extent = {{-150, -150}, {350, 150}}), graphics = {Rectangle(origin = {-2, 0},lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-150, -150}, {350, 150}}, radius = 25), Rectangle(origin = {93, 4}, fillColor = {255, 255, 0}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, 100}, {100, -100}}), Text(origin = {98, -172}, extent = {{-250, 42}, {250, -42}}, textString = "%name"), Ellipse(origin = {-8, 4}, fillColor = {255, 255, 0}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, extent = {{10, 100}, {-10, -100}}), Ellipse(origin = {242, -66}, fillColor = {0, 0, 186}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{3, 15}, {-3, -15}}), Ellipse(origin = {192, 4}, fillColor = {182, 182, 0}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{10, 100}, {-10, -100}}), Ellipse(origin = {242, 74}, fillColor = {0, 0, 186}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{3, 15}, {-3, -15}}), Rectangle(origin = {217, -66}, fillColor = {0, 0, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-25, 15}, {25, -15}}), Ellipse(origin = {192, 74}, fillColor = {0, 0, 255}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, extent = {{3, 15}, {-3, -15}}), Rectangle(origin = {217, 74}, fillColor = {0, 0, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-25, 15}, {25, -15}}), Ellipse(origin = {192, 4}, fillColor = {0, 0, 255}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, extent = {{3, 15}, {-3, -15}}), Rectangle(origin = {217, 4}, fillColor = {0, 0, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-25, 15}, {25, -15}}), Ellipse(origin = {242, 4}, fillColor = {0, 0, 186}, pattern = LinePattern.None, fillPattern = FillPattern.Solid, extent = {{3, 15}, {-3, -15}}), Ellipse(origin = {192, -66}, fillColor = {0, 0, 255}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, extent = {{3, 15}, {-3, -15}})}),
    Diagram(coordinateSystem(extent = {{-150, -150}, {350, 150}})));
end ATS;
