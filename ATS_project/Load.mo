within ATS_project;

model Load "La charge reliée à l'ATS"
  parameter Real R(displayUnit="Ohm") = 1000 "Valeur de la résistance de la charge ";
  Modelica.Electrical.Analog.Basic.Resistor resistor(R = R)  annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_p "Pin positif de la résistance" annotation(
    Placement(visible = true, transformation(origin = {-104, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_n "Pin négatif de la résistance" annotation(
    Placement(visible = true, transformation(origin = {104, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(pin_n, resistor.n) annotation(
    Line(points = {{104, 0}, {10, 0}}, color = {0, 0, 255}));
  connect(resistor.p, pin_p) annotation(
    Line(points = {{-10, 0}, {-104, 0}}, color = {0, 0, 255}));

annotation(
    Icon(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(visible = false, points = {{0, -100}, {0, -30}}, color = {127, 0, 0}, pattern = LinePattern.Dot), Rectangle(lineColor = {0, 0, 255}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-70, 30}, {70, -30}}), Line(points = {{70, 0}, {90, 0}}, color = {0, 0, 255}), Line(points = {{-90, 0}, {-70, 0}}, color = {0, 0, 255}), Text(origin = {0, -110}, extent = {{-100, 20}, {100, -20}}, textString = "%name")}));
end Load;
