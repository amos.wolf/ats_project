within ATS_project.Components;

model Solenoide_alt "Solenoide electrique"
  parameter Modelica.Units.SI.Resistance R(displayUnit = "Ohm") = 1 "resistance du solenoide";
  parameter Real Gain_force_solenoide=-20;
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_solenoide_p "solenoide borne positive" annotation(
    Placement(visible = true, transformation(origin = {-104, 46}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_solenoide_n "solenoide borne negative" annotation(
    Placement(visible = true, transformation(origin = {-106, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Translational.Interfaces.Flange_a flange_a annotation(
    Placement(visible = true, transformation(origin = {102, -12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Translational.Sources.Force force "force interne" annotation(
    Placement(visible = true, transformation(origin = {54, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Math.MultiProduct multiProduct(nu = 2) annotation(
    Placement(visible = true, transformation(origin = {-18, 22}, extent = {{-6, -6}, {6, 6}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant const(k = Gain_force_solenoide) "gain"  annotation(
    Placement(visible = true, transformation(origin = {-58, -12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor "capteur de tension" annotation(
    Placement(visible = true, transformation(origin = {-116, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Resistor resistor(R = R)  "resistance" annotation(
    Placement(visible = true, transformation(origin = {-78, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(force.flange, flange_a) annotation(
    Line(points = {{64, -10}, {102, -10}, {102, -12}}, color = {0, 127, 0}));
  connect(const.y, multiProduct.u[1]) annotation(
    Line(points = {{-46, -12}, {-24, -12}, {-24, 22}}, color = {0, 0, 127}));
  connect(multiProduct.y, force.f) annotation(
    Line(points = {{-10, 22}, {42, 22}, {42, -10}}, color = {0, 0, 127}));
  connect(voltageSensor.p, pin_solenoide_p) annotation(
    Line(points = {{-126, 8}, {-126, 30}, {-104, 30}, {-104, 46}}, color = {0, 0, 255}));
  connect(voltageSensor.n, pin_solenoide_n) annotation(
    Line(points = {{-106, 8}, {-106, -50}}, color = {0, 0, 255}));
  connect(voltageSensor.v, multiProduct.u[2]) annotation(
    Line(points = {{-116, -3}, {-24, -3}, {-24, 22}}, color = {0, 0, 127}));
  connect(resistor.p, pin_solenoide_p) annotation(
    Line(points = {{-88, 26}, {-104, 26}, {-104, 46}}, color = {0, 0, 255}));
  connect(resistor.n, pin_solenoide_n) annotation(
    Line(points = {{-68, 26}, {-106, 26}, {-106, -50}}, color = {0, 0, 255}));
annotation(
    Icon(graphics = {Polygon(origin = {-3.75, 0}, fillColor = {160, 160, 164}, fillPattern = FillPattern.Solid, points = {{33.75, 50}, {-46.25, 50}, {-46.25, -50}, {33.75, -50}, {33.75, -30}, {-21.25, -30}, {-21.25, 30}, {33.75, 30}, {33.75, 50}}), Line(origin = {10.4708, 41.6771}, points = {{-64, -56}, {-78, -56}}), Ellipse(origin = {10.4708, 41.6771}, extent = {{-86, -60}, {-78, -52}}), Line(origin = {10.4708, 41.6771}, points = {{-64, -20}, {-32, -28}}), Rectangle(lineColor = {128, 128, 128}, extent = {{-100, -100}, {100, 100}}, radius = 25), Rectangle(origin = {62.5, 0}, fillColor = {160, 160, 164}, fillPattern = FillPattern.Solid, extent = {{-12.5, -50}, {12.5, 50}}), Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(origin = {10.4708, 41.6771}, points = {{-64, -20}, {-78, -20}}), Ellipse(origin = {10.4708, 41.6771}, extent = {{-86, -24}, {-78, -16}}), Polygon(origin = {-3.75, 0}, fillColor = {160, 160, 164}, fillPattern = FillPattern.Solid, points = {{33.75, 50}, {-46.25, 50}, {-46.25, -50}, {33.75, -50}, {33.75, -30}, {-21.25, -30}, {-21.25, 30}, {33.75, 30}, {33.75, 50}}), Rectangle(origin = {62.5, 0}, fillColor = {160, 160, 164}, fillPattern = FillPattern.Solid, extent = {{-12.5, -50}, {12.5, 50}}), Line(origin = {10.4708, 41.6771}, points = {{-64, -56}, {-78, -56}}), Line(origin = {10.4708, 41.6771}, points = {{-64, -56}, {-32, -64}}), Line(origin = {10.4708, 41.6771}, points = {{-64.1812, -31.6229}, {-32, -40}}), Ellipse(origin = {10.4708, 41.6771}, extent = {{-86, -24}, {-78, -16}}), Line(origin = {10.4708, 41.6771}, points = {{-64, -20}, {-78, -20}}), Ellipse(origin = {10.4708, 41.6771}, extent = {{-86, -60}, {-78, -52}}), Rectangle(lineColor = {128, 128, 128}, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(origin = {10.4708, 41.6771}, points = {{-64, -44}, {-32, -52}}), Line(origin = {10.4708, 41.6771}, points = {{-64, -20}, {-32, -28}}), Text(origin = {0, -111}, extent = {{-100, 19}, {100, -19}}, textString = "%name")}));
end Solenoide_alt;
