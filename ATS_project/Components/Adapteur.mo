within ATS_project.Components;

model Adapteur "rileve l'angle de rotation en permettant de definir si l'alimentation est normal ou d'emergence"
  parameter Modelica.Units.SI.Torque Torque_threshold(displayUnit="N.m")=0;
  parameter Modelica.Units.SI.Angle Angle_threshold(displayUnit="rad")=1;
  parameter Modelica.Units.SI.Inertia Inertia_adapteur(displayUnit="kg.m2")=0.1;
  parameter Modelica.Units.SI.Force Brake_force(displayUnit="N")=10000;
  parameter Modelica.Units.SI.Angle Start_angle(displayUnit="rad")=0.52;
  Modelica.Blocks.Interfaces.BooleanOutput em_norm_decision annotation(
    Placement(visible = true, transformation(origin = {126, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Sensors.AngleSensor angleSensor annotation(
    Placement(visible = true, transformation(origin = {-28, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Logical.GreaterThreshold greaterThreshold(threshold = Angle_threshold)  annotation(
    Placement(visible = true, transformation(origin = {6, 12}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Logical.Switch switch1 annotation(
    Placement(visible = true, transformation(origin = {24, -80}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Blocks.Sources.Constant const(k = 0)  annotation(
    Placement(visible = true, transformation(origin = {-12, -70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Constant constant1(k = 1) annotation(
    Placement(visible = true, transformation(origin = {74, -66}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Brake brake(fn_max = Brake_force)  annotation(
    Placement(visible = true, transformation(origin = {-56, 4}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Interfaces.Flange_a flange_a annotation(
    Placement(visible = true, transformation(origin = {-146, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(extent = {{-120, -10}, {-100, 10}}, rotation = 0)));
  Modelica.Blocks.Logical.And and1 annotation(
    Placement(visible = true, transformation(origin = {50, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Logical.GreaterThreshold greaterThreshold1(threshold = Torque_threshold) annotation(
    Placement(visible = true, transformation(origin = {4, 46}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Sensors.TorqueSensor torqueSensor annotation(
    Placement(visible = true, transformation(origin = {-122, 2}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
  Modelica.Blocks.Logical.LessThreshold lessThreshold(threshold = -Angle_threshold)  annotation(
    Placement(visible = true, transformation(origin = {6, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Logical.And and11 annotation(
    Placement(visible = true, transformation(origin = {50, -34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Logical.LessThreshold lessThreshold1(threshold = Torque_threshold)  annotation(
    Placement(visible = true, transformation(origin = {-78, -48}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Logical.Or or1 annotation(
    Placement(visible = true, transformation(origin = {76, -8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Inertia inertia(J = Inertia_adapteur, phi(displayUnit = "deg",fixed = true, start = Start_angle), w(fixed = true, start = 2))  annotation(
    Placement(visible = true, transformation(origin = {-84, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(angleSensor.phi, greaterThreshold.u) annotation(
    Line(points = {{-17, 10}, {-12.5, 10}, {-12.5, 12}, {-6, 12}}, color = {0, 0, 127}));
  connect(const.y, switch1.u3) annotation(
    Line(points = {{-1, -70}, {15, -70}, {15, -68}}, color = {0, 0, 127}));
  connect(constant1.y, switch1.u1) annotation(
    Line(points = {{63, -66}, {47, -66}, {47, -68}, {31, -68}}, color = {0, 0, 127}));
  connect(brake.flange_b, angleSensor.flange) annotation(
    Line(points = {{-46, 4}, {-38, 4}, {-38, 10}}));
  connect(brake.f_normalized, switch1.y) annotation(
    Line(points = {{-56, -6}, {-50, -6}, {-50, -91}, {24, -91}}, color = {0, 0, 127}));
  connect(greaterThreshold1.y, and1.u1) annotation(
    Line(points = {{15, 46}, {38.5, 46}, {38.5, 34}, {38, 34}}, color = {255, 0, 255}));
  connect(flange_a, torqueSensor.flange_a) annotation(
    Line(points = {{-146, 0}, {-132, 0}, {-132, 2}}));
  connect(torqueSensor.tau, greaterThreshold1.u) annotation(
    Line(points = {{-130, 14}, {-126, 14}, {-126, 46}, {-8, 46}}, color = {0, 0, 127}));
  connect(lessThreshold.u, angleSensor.phi) annotation(
    Line(points = {{-6, -24}, {-16, -24}, {-16, 10}}, color = {0, 0, 127}));
  connect(greaterThreshold.y, and1.u2) annotation(
    Line(points = {{17, 12}, {38, 12}, {38, 26}}, color = {255, 0, 255}));
  connect(and11.u1, lessThreshold.y) annotation(
    Line(points = {{38, -34}, {18, -34}, {18, -24}}, color = {255, 0, 255}));
  connect(torqueSensor.tau, lessThreshold1.u) annotation(
    Line(points = {{-130, 14}, {-90, 14}, {-90, -48}}, color = {0, 0, 127}));
  connect(lessThreshold1.y, and11.u2) annotation(
    Line(points = {{-66, -48}, {38, -48}, {38, -42}}, color = {255, 0, 255}));
  connect(and1.y, or1.u1) annotation(
    Line(points = {{62, 34}, {62, 13}, {64, 13}, {64, -8}}, color = {255, 0, 255}));
  connect(and11.y, or1.u2) annotation(
    Line(points = {{62, -34}, {64, -34}, {64, -16}}, color = {255, 0, 255}));
  connect(or1.y, switch1.u2) annotation(
    Line(points = {{88, -8}, {24, -8}, {24, -68}}, color = {255, 0, 255}));
  connect(greaterThreshold.y, em_norm_decision) annotation(
    Line(points = {{18, 12}, {126, 12}, {126, -2}}, color = {255, 0, 255}));
  connect(inertia.flange_b, brake.flange_a) annotation(
    Line(points = {{-74, 2}, {-66, 2}, {-66, 4}}));
  connect(torqueSensor.flange_b, inertia.flange_a) annotation(
    Line(points = {{-112, 2}, {-94, 2}}));
annotation(
    Icon(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Rectangle(origin = {-10, 0}, lineColor = {64, 64, 64}, fillColor = {192, 192, 192}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -10}, {-30, 10}}), Line(visible = false, origin = {-10.534, -0.339806}, points = {{-100, -100}, {-100, -40}, {0, -40}}, color = {191, 0, 0}, pattern = LinePattern.Dot), Rectangle(origin = {-10, 0}, lineColor = {64, 64, 64}, extent = {{-30, -60}, {-10, 60}}), Rectangle(origin = {-10, 0}, lineColor = {64, 64, 64}, fillColor = {255, 255, 255}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-30, -60}, {-10, 60}}), Rectangle(origin = {49, 0}, fillColor = {255, 0, 255}, fillPattern = FillPattern.Solid, extent = {{-51, 4}, {51, -4}}), Text(origin = {0, -113}, extent = {{-120, 25}, {120, -25}}, textString = "%name")}));
end Adapteur;
