within ATS_project.Components;

model Controlleur "Il permet de decider quelle source il faut utiliser"
  parameter Real V1_min(displayUnit="V") = 3 "Tension au dessous duquel on passe a emergency ";
  parameter Real V2_min(displayUnit="V") = 5 "Tension au dessus duquel on repasse a normal ";
  parameter Modelica.Units.SI.Time tc = 0.1 "Temps de delai ";
  Modelica.Blocks.Interfaces.BooleanOutput y "Controle du Soleinoide Switch" annotation(
    Placement(visible = true, transformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.RealInput sensor "Voltage Sensor" annotation(
    Placement(visible = true, transformation(origin = {-122, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Blocks.MathBoolean.RisingEdge rising1 annotation(
    Placement(visible = true, transformation(origin = {-32, 36}, extent = {{-4, -4}, {4, 4}}, rotation = 0)));
  Modelica.Blocks.Logical.LessEqualThreshold lessEqualThreshold(threshold = V1_min)  annotation(
    Placement(visible = true, transformation(origin = {-64, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Logical.LogicalDelay logicalDelay(delayTime = tc)  annotation(
    Placement(visible = true, transformation(origin = {46, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Logical.Not not1 annotation(
    Placement(visible = true, transformation(origin = {82, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualThreshold(threshold = V2_min)  annotation(
    Placement(visible = true, transformation(origin = {-74, -36}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.MathBoolean.RisingEdge risingEdge(pre_u_start = true)  annotation(
    Placement(visible = true, transformation(origin = {-26, -14}, extent = {{-4, -4}, {4, 4}}, rotation = 0)));
  Modelica.Blocks.Logical.Or or1 annotation(
    Placement(visible = true, transformation(origin = {8, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(lessEqualThreshold.u, sensor) annotation(
    Line(points = {{-76, 14}, {-77, 14}, {-77, 0}, {-122, 0}}, color = {0, 0, 127}));
  connect(logicalDelay.y1, not1.u) annotation(
    Line(points = {{57, 10}, {70, 10}}, color = {255, 0, 255}));
  connect(not1.y, y) annotation(
    Line(points = {{93, 10}, {110, 10}, {110, 0}}, color = {255, 0, 255}));
  connect(sensor, greaterEqualThreshold.u) annotation(
    Line(points = {{-122, 0}, {-86, 0}, {-86, -36}}, color = {0, 0, 127}));
  connect(or1.y, logicalDelay.u) annotation(
    Line(points = {{19, 4}, {34, 4}}, color = {255, 0, 255}));
  connect(lessEqualThreshold.y, rising1.u) annotation(
    Line(points = {{-52, 14}, {-38, 14}, {-38, 36}}, color = {255, 0, 255}));
  connect(or1.u1, rising1.y) annotation(
    Line(points = {{-4, 4}, {-18, 4}, {-18, 36}, {-28, 36}}, color = {255, 0, 255}));
  connect(greaterEqualThreshold.y, risingEdge.u) annotation(
    Line(points = {{-62, -36}, {-32, -36}, {-32, -14}}, color = {255, 0, 255}));
  connect(risingEdge.y, or1.u2) annotation(
    Line(points = {{-22, -14}, {-4, -14}, {-4, -4}}, color = {255, 0, 255}));
  annotation(
    Icon(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(origin = {79, 30}, points = {{-84, -6}, {-37, -6}}), Line(origin = {7, 47}, points = {{-84, -6}, {-52, -6}}), Rectangle(lineColor = {128, 128, 128}, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(points = {{42, -12}, {17, -12}, {17, -54}, {-71, -54}}), Rectangle(origin = {59, 53}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-104, -63}, {-64, 7}}), Line(origin = {7, 15}, points = {{-84, -6}, {-52, -6}}), Rectangle(origin = {146, 34}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Solid, extent = {{-104, -63}, {-64, 7}}), Text(origin = {0, -109}, extent = {{-100, 19}, {100, -19}}, textString = "%name")}));
end Controlleur;
