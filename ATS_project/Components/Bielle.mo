within ATS_project.Components;

model Bielle
  parameter Real R=0.1 "rayon de la bielle";
  Real rho;
  Modelica.Mechanics.Translational.Interfaces.Flange_b flange_b annotation(
    Placement(visible = true, transformation(origin = {-100, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-110, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Interfaces.Flange_a flange_a annotation(
    Placement(visible = true, transformation(origin = {110, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {50, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  flange_b.s  = R * (cos(flange_a.phi ) - 1);
  rho = -R * sin(flange_a.phi );
  0 = flange_a.tau - flange_b.f * rho;
annotation(
    Icon(graphics = {Bitmap(extent = {{-14, -36}, {-14, -36}}), Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Ellipse(origin = {50, 0}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Sphere, extent = {{50, 50}, {-50, -50}}), Polygon(origin = {-27, 19}, fillColor = {255, 255, 255}, fillPattern = FillPattern.Sphere, points = {{73, 9}, {73, 29}, {-73, -9}, {-73, -29}, {73, 9}}), Ellipse(origin = {47, 37}, fillColor = {77, 77, 77}, fillPattern = FillPattern.Solid, extent = {{11, 11}, {-11, -11}}), Text(origin = {0, -109}, extent = {{-100, 19}, {100, -19}}, textString = "%name")}));
end Bielle;
