within ATS_project.Components;

model Switch "switch entre source normale et d'emergence"
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_emergency annotation(
    Placement(visible = true, transformation(origin = {100, 76}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 90}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_load annotation(
    Placement(visible = true, transformation(origin = {100, 6}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 10}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_normal annotation(
    Placement(visible = true, transformation(origin = {100, -84}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, -64}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Interfaces.BooleanInput em_norm_decision "decision si normal ou emergency" annotation(
    Placement(visible = true, transformation(origin = {-122, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0), iconTransformation(origin = {-120, 0}, extent = {{-20, -20}, {20, 20}}, rotation = 0)));
  Modelica.Electrical.Analog.Ideal.IdealOpeningSwitch switch annotation(
    Placement(visible = true, transformation(origin = {-18, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Ideal.IdealOpeningSwitch switch1 annotation(
    Placement(visible = true, transformation(origin = {-10, -60}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Logical.Not not1 annotation(
    Placement(visible = true, transformation(origin = {-60, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_normal_n annotation(
    Placement(visible = true, transformation(origin = {98, -56}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, -88}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_emergency_n annotation(
    Placement(visible = true, transformation(origin = {98, 46}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, 68}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_load_n annotation(
    Placement(visible = true, transformation(origin = {100, -18}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {110, -16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation(
    Placement(visible = true, transformation(origin = {20, -16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(switch.n, pin_emergency) annotation(
    Line(points = {{-8, 26}, {12, 26}, {12, 76}, {100, 76}}, color = {0, 0, 255}));
  connect(switch.p, pin_load) annotation(
    Line(points = {{-28, 26}, {-28, 6}, {100, 6}}, color = {0, 0, 255}));
  connect(switch1.n, pin_normal) annotation(
    Line(points = {{0, -60}, {8, -60}, {8, -84}, {100, -84}}, color = {0, 0, 255}));
  connect(switch1.p, pin_load) annotation(
    Line(points = {{-20, -60}, {-28, -60}, {-28, 6}, {100, 6}}, color = {0, 0, 255}));
  connect(pin_emergency_n, pin_normal_n) annotation(
    Line(points = {{98, 46}, {64, 46}, {64, -56}, {98, -56}}, color = {0, 0, 255}));
  connect(pin_load_n, pin_normal_n) annotation(
    Line(points = {{100, -18}, {64, -18}, {64, -56}, {98, -56}}, color = {0, 0, 255}));
  connect(em_norm_decision, not1.u) annotation(
    Line(points = {{-122, 0}, {-72, 0}}, color = {255, 0, 255}));
  connect(not1.y, switch1.control) annotation(
    Line(points = {{-49, 0}, {-10, 0}, {-10, -48}}, color = {255, 0, 255}));
  connect(switch.control, em_norm_decision) annotation(
    Line(points = {{-18, 38}, {-82, 38}, {-82, 0}, {-122, 0}}, color = {255, 0, 255}));
  connect(ground.p, pin_load_n) annotation(
    Line(points = {{20, -6}, {100, -6}, {100, -18}}, color = {0, 0, 255}));

annotation(
    Icon(graphics = {Line(points = {{40, 0}, {90, 0}}), Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(points = {{-40, 0}, {32, 60}}), Rectangle(lineColor = {128, 128, 128}, extent = {{-100, -100}, {100, 100}}, radius = 25), Line(points = {{-90, 0}, {-40, 0}}), Line(points = {{40, 0}, {90, 0}}), Line(points = {{-40, 0}, {32, 60}}), Text(origin = {-5, -116}, extent = {{-89, 32}, {89, -32}}, textString = "%name")}));
end Switch;
