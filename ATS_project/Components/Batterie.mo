within ATS_project.Components;

model Batterie "batterie qui alimente le solenoide"
  parameter Modelica.Units.SI.Voltage V_batterie(displayUnit = "V") = 5;
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(V = V_batterie)  annotation(
    Placement(visible = true, transformation(origin = {-22, 32}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.PositivePin pin_p annotation(
    Placement(visible = true, transformation(origin = {-42, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-90, 110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Interfaces.NegativePin pin_n annotation(
    Placement(visible = true, transformation(origin = {32, -10}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {90, 110}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(pin_n, constantVoltage.n) annotation(
    Line(points = {{32, -10}, {-12, -10}, {-12, 32}}, color = {0, 0, 255}));
  connect(pin_p, constantVoltage.p) annotation(
    Line(points = {{-42, -10}, {-32, -10}, {-32, 32}}, color = {0, 0, 255}));
annotation(
    Icon(graphics = {Rectangle(lineColor = {200, 200, 200}, fillColor = {248, 248, 248}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-100, -100}, {100, 100}}, radius = 25), Rectangle(origin = {-7.10543e-15, -10}, rotation = 90, lineColor = {95, 95, 95}, fillColor = {215, 215, 215}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-70, 50}, {70, -50}}), Ellipse(origin = {0, -80}, rotation = 90, lineColor = {95, 95, 95}, fillColor = {215, 215, 215}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-10, 50}, {10, -50}}), Rectangle(origin = {0, -10}, rotation = 90, lineColor = {95, 95, 95}, fillColor = {215, 215, 215}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, extent = {{-70, 50}, {70, -50}}), Ellipse(origin = {0, 60}, rotation = 90, lineColor = {95, 95, 95}, fillColor = {215, 215, 215}, fillPattern = FillPattern.Sphere, extent = {{-10, 50}, {10, -50}}), Ellipse(origin = {0, 60}, rotation = 90, lineColor = {95, 95, 95}, fillColor = {215, 215, 215}, fillPattern = FillPattern.HorizontalCylinder, extent = {{-4, 20}, {4, -20}}), Rectangle(origin = {0, 73}, rotation = 90, lineColor = {95, 95, 95}, fillColor = {215, 215, 215}, pattern = LinePattern.None, fillPattern = FillPattern.HorizontalCylinder, extent = {{-13, 20}, {13, -20}}), Ellipse(origin = {0, 86}, rotation = 90, lineColor = {95, 95, 95}, fillColor = {215, 215, 215}, fillPattern = FillPattern.Sphere, extent = {{-4, 20}, {4, -20}}), Text(origin = {0, -115}, extent = {{-100, 27}, {100, -27}}, textString = "%name")}));
end Batterie;
