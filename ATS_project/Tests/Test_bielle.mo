within ATS_project.Tests;

model Test_bielle
  ATS_project.Components.Bielle bielle annotation(
    Placement(visible = true, transformation(origin = {-28, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Translational.Components.Mass mass(m = 0.01, s(start = 0))  annotation(
    Placement(visible = true, transformation(origin = {-52, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ATS_project.Components.Adapteur adapteur annotation(
    Placement(visible = true, transformation(origin = {22, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Translational.Components.Fixed fixed(s0 = 0)  annotation(
    Placement(visible = true, transformation(origin = {-86, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Translational.Components.Spring spring(c = 10, s_rel0 = 2)  annotation(
    Placement(visible = true, transformation(origin = {-70, -24}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Translational.Sources.Force force annotation(
    Placement(visible = true, transformation(origin = {-80, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Pulse pulse(amplitude = -100, period = 5, startTime = 1, width = 3)  annotation(
    Placement(visible = true, transformation(origin = {-130, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Inertia inertia(J = 0.1)  annotation(
    Placement(visible = true, transformation(origin = {-4, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(mass.flange_b, bielle.flange_b) annotation(
    Line(points = {{-42, 2}, {-38, 2}, {-38, 0}}, color = {0, 127, 0}));
  connect(spring.flange_b, mass.flange_a) annotation(
    Line(points = {{-60, -24}, {-62, -24}, {-62, 2}}, color = {0, 127, 0}));
  connect(fixed.flange, spring.flange_a) annotation(
    Line(points = {{-86, -40}, {-80, -40}, {-80, -24}}, color = {0, 127, 0}));
  connect(force.flange, mass.flange_a) annotation(
    Line(points = {{-70, 4}, {-62, 4}, {-62, 2}}, color = {0, 127, 0}));
  connect(pulse.y, force.f) annotation(
    Line(points = {{-118, 14}, {-92, 14}, {-92, 4}}, color = {0, 0, 127}));
  connect(bielle.flange_a, inertia.flange_a) annotation(
    Line(points = {{-18, 0}, {-14, 0}}));
  connect(inertia.flange_b, adapteur.flange_a) annotation(
    Line(points = {{6, 0}, {8, 0}}));
protected
  annotation(
    experiment(StartTime = 0, StopTime = 10, Tolerance = 1e-6, Interval = 0.02));
end Test_bielle;
