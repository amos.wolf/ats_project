within ATS_project.Tests;

model Controlleur_test
  ATS_project.Components.Controlleur controlleur annotation(
    Placement(visible = true, transformation(origin = {-46, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Ideal.IdealOpeningSwitch switch annotation(
    Placement(visible = true, transformation(origin = {6, -16}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ATS_project.Components.Solenoide_alt solenoide_alt annotation(
    Placement(visible = true, transformation(origin = {10, -78}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Translational.Components.Mass mass annotation(
    Placement(visible = true, transformation(origin = {58, -50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Components.Batterie batterie annotation(
    Placement(visible = true, transformation(origin = {-78, -74}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation(
    Placement(visible = true, transformation(origin = {-50, -100}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Sine sine(amplitude = 10, f = 0.5, offset = 1)  annotation(
    Placement(visible = true, transformation(origin = {-96, 6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(controlleur.y, switch.control) annotation(
    Line(points = {{-34, 0}, {6, 0}, {6, -4}}, color = {255, 0, 255}));
  connect(mass.flange_a, solenoide_alt.flange_a) annotation(
    Line(points = {{48, -50}, {20, -50}, {20, -79}}, color = {0, 127, 0}));
  connect(switch.p, batterie.pin_p) annotation(
    Line(points = {{-4, -16}, {-86, -16}, {-86, -62}}, color = {0, 0, 255}));
  connect(batterie.pin_n, solenoide_alt.pin_solenoide_n) annotation(
    Line(points = {{-68, -62}, {-42, -62}, {-42, -82}, {0, -82}}, color = {0, 0, 255}));
  connect(solenoide_alt.pin_solenoide_p, switch.n) annotation(
    Line(points = {{0, -72}, {16, -72}, {16, -16}}, color = {0, 0, 255}));
  connect(ground.p, batterie.pin_n) annotation(
    Line(points = {{-50, -90}, {-68, -90}, {-68, -62}}, color = {0, 0, 255}));
  connect(sine.y, controlleur.sensor) annotation(
    Line(points = {{-84, 6}, {-58, 6}, {-58, 0}}, color = {0, 0, 127}));
annotation(
    experiment(StartTime = 0, StopTime = 10, Tolerance = 1e-6, Interval = 0.02));
end Controlleur_test;
