within ATS_project.Tests;

model Test_soleinoide_alt
  ATS_project.Components.Solenoide_alt solenoide_alt annotation(
    Placement(visible = true, transformation(origin = {14, -4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation(
    Placement(visible = true, transformation(origin = {-32, -32}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Components.Batterie batterie annotation(
    Placement(visible = true, transformation(origin = {-78, -2}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Mechanics.Translational.Components.Mass mass annotation(
    Placement(visible = true, transformation(origin = {44, -4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(ground.p, solenoide_alt.pin_solenoide_n) annotation(
    Line(points = {{-32, -22}, {-32, -9}, {3, -9}}, color = {0, 0, 255}));
  connect(batterie.pin_n, solenoide_alt.pin_solenoide_n) annotation(
    Line(points = {{-66, -10}, {-22, -10}, {-22, -9}, {3, -9}}, color = {0, 0, 255}));
  connect(batterie.pin_p, solenoide_alt.pin_solenoide_p) annotation(
    Line(points = {{-66, 8}, {3, 8}, {3, 1}}, color = {0, 0, 255}));
  connect(solenoide_alt.flange_a, mass.flange_a) annotation(
    Line(points = {{25, -4}, {34, -4}}, color = {0, 127, 0}));
end Test_soleinoide_alt;
