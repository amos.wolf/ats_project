within ATS_project.Tests;

model Switch_test
  ATS_project.Components.Switch switch annotation(
    Placement(visible = true, transformation(origin = {0, 0}, extent = {{-38, -38}, {38, 38}}, rotation = 0)));
  Modelica.Blocks.Sources.Pulse pulse(period = 1)  annotation(
    Placement(visible = true, transformation(origin = {-104, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ATS_project.Load load annotation(
    Placement(visible = true, transformation(origin = {102, -6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ATS_project.Normal normal(V(displayUnit = ""), width = 100)  annotation(
    Placement(visible = true, transformation(origin = {100, -68}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  ATS_project.Emergency emergency annotation(
    Placement(visible = true, transformation(origin = {98, 70}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  Modelica.Blocks.Logical.GreaterEqualThreshold greaterEqualThreshold(threshold = 0.2)  annotation(
    Placement(visible = true, transformation(origin = {-72, -34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(switch.pin_load, load.pin_p) annotation(
    Line(points = {{42, 0}, {68, 0}, {68, -2}, {91, -2}}, color = {0, 0, 255}));
  connect(switch.pin_load_n, load.pin_n) annotation(
    Line(points = {{42, -10}, {91, -10}, {91, -9}}, color = {0, 0, 255}));
  connect(normal.pin_p, switch.pin_normal) annotation(
    Line(points = {{90, -64}, {82, -64}, {82, -24}, {42, -24}}, color = {0, 0, 255}));
  connect(normal.pin_n, switch.pin_normal_n) annotation(
    Line(points = {{90, -74}, {64, -74}, {64, -34}, {42, -34}}, color = {0, 0, 255}));
  connect(switch.pin_emergency_n, emergency.pin_n) annotation(
    Line(points = {{42, 26}, {88, 26}, {88, 66}}, color = {0, 0, 255}));
  connect(switch.pin_emergency, emergency.pin_p) annotation(
    Line(points = {{42, 34}, {72, 34}, {72, 72}, {88, 72}}, color = {0, 0, 255}));
  connect(greaterEqualThreshold.y, switch.em_norm_decision) annotation(
    Line(points = {{-61, -34}, {-46, -34}, {-46, 0}}, color = {255, 0, 255}));
  connect(greaterEqualThreshold.u, pulse.y) annotation(
    Line(points = {{-84, -34}, {-88, -34}, {-88, -6}, {-92, -6}}, color = {0, 0, 127}));
  connect(greaterEqualThreshold.y, switch.em_norm_decision) annotation(
    Line(points = {{-60, -34}, {-46, -34}, {-46, 0}}, color = {255, 0, 255}));
annotation(
    experiment(StartTime = 0, StopTime = 10, Tolerance = 1e-6, Interval = 0.02));
end Switch_test;
