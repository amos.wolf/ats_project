within ATS_project.Tests;

model Test_solenoide
  Modelica.Magnetic.FluxTubes.Examples.SolenoidActuator.Components.SimpleSolenoid simpleSolenoid(x(start = 0.03))  annotation(
    Placement(visible = true, transformation(origin = {6, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Translational.Components.Mass mass(m = 1, s(fixed = true, start = 0.01))  annotation(
    Placement(visible = true, transformation(origin = {38, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage annotation(
    Placement(visible = true, transformation(origin = {-90, 14}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation(
    Placement(visible = true, transformation(origin = {-78, -38}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(mass.flange_a, simpleSolenoid.flange) annotation(
    Line(points = {{28, 4}, {22, 4}, {22, 8}, {16, 8}}, color = {0, 127, 0}));
  connect(constantVoltage.p, simpleSolenoid.p) annotation(
    Line(points = {{-90, 24}, {-44, 24}, {-44, 18}, {-4, 18}}, color = {0, 0, 255}));
  connect(constantVoltage.n, simpleSolenoid.n) annotation(
    Line(points = {{-90, 4}, {-44, 4}, {-44, -2}, {-4, -2}}, color = {0, 0, 255}));
  connect(ground.p, constantVoltage.n) annotation(
    Line(points = {{-78, -28}, {-90, -28}, {-90, 4}}, color = {0, 0, 255}));
end Test_solenoide;
