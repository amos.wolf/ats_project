within ATS_project.Tests;

model Test_ats_simplified
  Modelica.Mechanics.Translational.Components.Spring spring(c = 10, s_rel(start = 0), s_rel0 = 2) annotation(
    Placement(visible = true, transformation(origin = {34, -20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ATS_project.Components.Controlleur controlleur annotation(
    Placement(visible = true, transformation(origin = {-55, 77}, extent = {{-19, -19}, {19, 19}}, rotation = 0)));
  Modelica.Mechanics.Translational.Components.Fixed fixed annotation(
    Placement(visible = true, transformation(origin = {14, -26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Components.Solenoide_alt solenoide_alt annotation(
    Placement(visible = true, transformation(origin = {14, 6}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Components.Batterie batterie annotation(
    Placement(visible = true, transformation(origin = {-86, 6}, extent = {{-18, -18}, {18, 18}}, rotation = -90)));
  Modelica.Electrical.Analog.Ideal.IdealOpeningSwitch switch1 annotation(
    Placement(visible = true, transformation(origin = {-16, 34}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Translational.Components.Mass mass(m = 0.01) annotation(
    Placement(visible = true, transformation(origin = {52, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Basic.Ground ground annotation(
    Placement(visible = true, transformation(origin = {-62, -40}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ATS_project.Components.Bielle bielle annotation(
    Placement(visible = true, transformation(origin = {118, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Mechanics.Rotational.Components.Inertia inertia(J = 0.1) annotation(
    Placement(visible = true, transformation(origin = {144, 2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ATS_project.Components.Adapteur adapteur annotation(
    Placement(visible = true, transformation(origin = {166, -32}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ATS_project.Components.Switch switch annotation(
    Placement(visible = true, transformation(origin = {250, 18}, extent = {{-38, -38}, {38, 38}}, rotation = 0)));
  ATS_project.Load load annotation(
    Placement(visible = true, transformation(origin = {350, -2}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  ATS_project.Normal normal annotation(
    Placement(visible = true, transformation(origin = {348, -64}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  ATS_project.Emergency emergency annotation(
    Placement(visible = true, transformation(origin = {346, 74}, extent = {{10, -10}, {-10, 10}}, rotation = 0)));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor annotation(
    Placement(visible = true, transformation(origin = {300, -88}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
equation
  connect(solenoide_alt.pin_solenoide_p, switch1.n) annotation(
    Line(points = {{4, 12}, {-6, 12}, {-6, 34}}, color = {0, 0, 255}));
  connect(solenoide_alt.pin_solenoide_n, batterie.pin_n) annotation(
    Line(points = {{4, 2}, {-66, 2}, {-66, -10}}, color = {0, 0, 255}));
  connect(controlleur.y, switch1.control) annotation(
    Line(points = {{-34, 77}, {-34, 59.5}, {-16, 59.5}, {-16, 46}}, color = {255, 0, 255}));
  connect(batterie.pin_p, switch1.p) annotation(
    Line(points = {{-66, 22}, {-26, 22}, {-26, 34}}, color = {0, 0, 255}));
  connect(spring.flange_a, fixed.flange) annotation(
    Line(points = {{24, -20}, {14, -20}, {14, -26}}, color = {0, 127, 0}));
  connect(spring.flange_b, mass.flange_a) annotation(
    Line(points = {{44, -20}, {42, -20}, {42, 0}}, color = {0, 127, 0}));
  connect(solenoide_alt.flange_a, mass.flange_a) annotation(
    Line(points = {{24, 4}, {42, 4}, {42, 0}}, color = {0, 127, 0}));
  connect(ground.p, batterie.pin_n) annotation(
    Line(points = {{-62, -30}, {-66, -30}, {-66, -10}}, color = {0, 0, 255}));
  connect(bielle.flange_a, inertia.flange_a) annotation(
    Line(points = {{128.4, 3.8}, {130.4, 3.8}, {130.4, 2}, {134, 2}}));
  connect(inertia.flange_b, adapteur.flange_a) annotation(
    Line(points = {{154, 2}, {155.5, 2}, {155.5, -32}, {155, -32}}));
  connect(bielle.flange_b, mass.flange_b) annotation(
    Line(points = {{108, 4}, {62, 4}, {62, 0}}, color = {0, 127, 0}));
  connect(switch.pin_emergency_n, emergency.pin_n) annotation(
    Line(points = {{292, 44}, {335.8, 44}, {335.8, 69.84}}, color = {0, 0, 255}));
  connect(normal.pin_n, switch.pin_normal_n) annotation(
    Line(points = {{337, -69.2}, {311, -69.2}, {311, -15}, {292, -15}}, color = {0, 0, 255}));
  connect(normal.pin_p, switch.pin_normal) annotation(
    Line(points = {{337.2, -59.4}, {329.2, -59.4}, {329.2, -6}, {292, -6}}, color = {0, 0, 255}));
  connect(switch.pin_emergency, emergency.pin_p) annotation(
    Line(points = {{292, 52}, {319.8, 52}, {319.8, 76.2}, {335.8, 76.2}}, color = {0, 0, 255}));
  connect(switch.pin_load, load.pin_p) annotation(
    Line(points = {{292, 22}, {340, 22}, {340, 2}}, color = {0, 0, 255}));
  connect(switch.pin_load_n, load.pin_n) annotation(
    Line(points = {{292, 12}, {317, 12}, {317, -4}, {340, -4}}, color = {0, 0, 255}));
  connect(voltageSensor.n, normal.pin_n) annotation(
    Line(points = {{310, -88}, {338, -88}, {338, -70}}, color = {0, 0, 255}));
  connect(voltageSensor.p, switch.pin_normal_n) annotation(
    Line(points = {{290, -88}, {290, -51.5}, {292, -51.5}, {292, -15}}, color = {0, 0, 255}));
  connect(voltageSensor.v, controlleur.sensor) annotation(
    Line(points = {{300, -98}, {-78, -98}, {-78, 78}}, color = {0, 0, 127}));
  connect(adapteur.em_norm_decision, switch.em_norm_decision) annotation(
    Line(points = {{177, -32}, {192.5, -32}, {192.5, 18}, {204, 18}}, color = {255, 0, 255}));
  annotation(
    Diagram(coordinateSystem(extent = {{-400, -400}, {400, 400}})),
    Icon(coordinateSystem(extent = {{-400, -400}, {400, 400}})));
end Test_ats_simplified;
