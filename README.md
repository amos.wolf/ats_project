# ATS_project

The ATS_project is a minimal simulation of an ATS (Automatic Transfert Switch), that is used in redondant electrical installations to prevent
interruptions in the power supply.

The code is organized in the `ATS_project` package which contains:

* the simulation model `ATS_Test`
* the main components of the simulation : 'ATS', 'Load', 'Normal', 'Emergency'
* a subpackage `Components` for the subcomponents of the ATS: 'Adapteur', 'Batterie', 'Bielle', 'Controlleur', 'Solenoide_alt', 'Switch'
* a subpackage `Tests` for standalone testing of some complex components like `Test_bielle`

The project is hosted in the public repository https://gitlab-student.centralesupelec.fr/amos.wolf/ats_project

## Design principles

It is intented as project for a course at CentraleSupélec called "Modelica et bond graph".
Here are some principles we targeted during the development.

* The package structure is reasonably "tidy"
* All models have a description string
* All parameters and variables have a physical unit and, when applicable, 
  a reasonable displayUnit

## Simulation

To run the simulation :
* Open package.mo
* In OMEdit, open ATS_Test
* Run simulation
* Display load/pin_p/v